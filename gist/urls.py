from django.conf.urls import patterns, include, url

from views import *

urlpatterns = patterns('',
    url(r'^$', list_view),
    url(r'^gist-(?P<gist_pk>\d+)/$', gist_view),
    url(r'^tag-(?P<tag_pk>\d+)/$', list_view),
)
