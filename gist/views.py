from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from models import Gist, Tag

def list_view(request, tag_pk=None, template="gist/home.html"):
    qs = Gist.objects.order_by("-pk")
    if tag_pk:
        qs = qs.filter(tags=tag_pk)
    return render_to_response(template,
        {
            "gist_qs": qs,
            "tag_qs": Tag.objects.order_by("name"),
        },
        context_instance=RequestContext(request))

def gist_view(request, gist_pk, template="gist/gist.html"):
    return render_to_response(template,
        {
            "gist": get_object_or_404(Gist, pk=gist_pk),
            "tag_qs": Tag.objects.order_by("name"),
        },
        context_instance=RequestContext(request))