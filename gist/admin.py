from django.contrib import admin

from models import Gist, File, UserProfile, Tag

class FileInline(admin.StackedInline):
    model = File
    extra = 1

class GistAdmin(admin.ModelAdmin):
    inlines = [
        FileInline,
    ]

class UserProfileAdmin(admin.ModelAdmin):
    pass

admin.site.register(Gist, GistAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Tag)