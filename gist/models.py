from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User)

    name = models.CharField(max_length=64)
    bio = models.TextField(blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    avatar = models.ImageField(upload_to="gist/user/avatar", blank=True, null=True)

    def __unicode__(self):
        return self.user.username

class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name    

class Gist(models.Model):
    author = models.ForeignKey(User)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    tags = models.ManyToManyField(Tag, blank=True, null=True)

    def __unicode__(self):
        return self.name

class File(models.Model):
    gist = models.ForeignKey(Gist)
    name = models.CharField(max_length=255)
    file_name = models.CharField(max_length=64, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    content = models.TextField()

    def __unicode__(self):
        return "%s %s" % (self.gist, self.name)